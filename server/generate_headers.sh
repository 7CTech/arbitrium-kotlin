#!/bin/bash
#echo "WIP DO NOT USE"
#exit 1

PROJECT_NAME="ArbitriumNativeClasses"

if [[ ! -f NativeClasses.txt ]]; then
    echo "Missing NativeClasses.txt"
    exit 1
fi

./gradlew jar
if [[ $? -ne 0 ]]; then
    echo "BUILD FAILED"
    exit 1
fi

mkdir -p src/nativemodules/cpp/gen-headers

cd build/classes/kotlin/main

for class in $(cat ../../../../NativeClasses.txt); do
    /usr/bin/env javah -d "../../../../src/nativemodules/cpp/gen-headers" "${class}"
done
