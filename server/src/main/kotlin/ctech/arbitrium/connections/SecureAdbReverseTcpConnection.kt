package ctech.arbitrium.connections

import ctech.arbitrium.common.SharedConstants
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory

class SecureAdbReverseTcpConnection(adbExec: String) : AdbReverseTcpConnection(adbExec, false) {

    private val serverSocket: ServerSocket
    private val socket: Socket
    private val sslContext: SSLContext

    init {
        initAdb()

        sslContext = SSLContext.getInstance("TLSv1.2") //LATEST AND GREATEST



        //sslContext.init()


        serverSocket = sslContext.serverSocketFactory.createServerSocket(SharedConstants.PORT)
        socket = serverSocket.accept()
    }

    override fun setTimeout(timeout: Int) {

    }

    override fun receive(): GenericPacket {
        if (socket.inputStream.available() != 0) throw Exception("stream has nothing")
        val data = BufferedReader(InputStreamReader(socket.inputStream)).readLine()
        if (data.isEmpty() || data.isBlank()) throw Exception("stream has nothing")
        return GenericPacket(data, address, SharedConstants.PORT)
    }

    override fun close() {
        socket.close()
    }

    override fun send(data: String, address: InetAddress, port: Int) {
        socket.outputStream?.write((data + "\n").toByteArray())
    }
}