package ctech.arbitrium.connections


import ctech.arbitrium.Connection
import ctech.arbitrium.common.SharedConstants
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

public class DatagramConnection : Connection() {
    private val socket = DatagramSocket(SharedConstants.PORT)

    override fun send(data: String, address: InetAddress, port: Int) {
        socket.send(DatagramPacket(data.toByteArray(), data.length, address, port))
    }

    override fun receive(): GenericPacket {
        val buf = ByteArray(SharedConstants.BUF_LEN)
        val packet = DatagramPacket(buf, SharedConstants.BUF_LEN)
        socket.receive(packet)
        return GenericPacket(String(packet.data, 0, packet.length), packet.address, packet.port)
    }

    override fun setTimeout(timeout: Int) {
        socket.soTimeout = timeout
    }

	public override fun close() {
        try {
            socket.close()
        } catch (e: Exception) {

        }

	}
}
