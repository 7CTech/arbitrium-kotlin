package ctech.arbitrium.connections

import ctech.arbitrium.Connection
import ctech.arbitrium.common.SharedConstants
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket

open class AdbReverseTcpConnection(private val adbExec: String, initSocket: Boolean = true) : Connection() {
    protected val address = InetAddress.getByName("127.0.0.1")
    private val serverSocket: ServerSocket?
    private val socket: Socket?

    protected fun initAdb() {
        try {
            Runtime.getRuntime().exec("$adbExec start-server").waitFor()
            if (Runtime.getRuntime().exec("$adbExec -d reverse tcp:${SharedConstants.PORT} tcp:${SharedConstants.PORT}").waitFor() != 0) {
                throw Exception()
            }
        } catch (e: Exception) {
            println("Failed to initialise adb")
            e.printStackTrace()
        }
        println("Adb initialised")
    }

    init {
        initAdb()

        if (initSocket) {
            serverSocket = ServerSocket(SharedConstants.PORT) // base impl
            socket = serverSocket.accept()
        } else {
            serverSocket = null
            socket = null
        }
    }

    override fun setTimeout(timeout: Int) {
        socket?.soTimeout = timeout
    }

    override fun receive(): GenericPacket {
        if (socket?.inputStream?.available() != 0) throw Exception("stream has nothing")
        val data = BufferedReader(InputStreamReader(socket.inputStream)).readLine()
        if (data.isEmpty() || data.isBlank()) throw Exception("stream has nothing")
        return GenericPacket(data, address, SharedConstants.PORT)
    }

    override fun close() {
        socket?.close()
    }

    override fun send(data: String, address: InetAddress, port: Int) {
        socket?.outputStream?.write((data + "\n").toByteArray())
    }
}
