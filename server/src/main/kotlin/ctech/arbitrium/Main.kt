package ctech.arbitrium

import com.google.gson.reflect.TypeToken
import ctech.arbitrium.common.Extensions.startThread
import ctech.arbitrium.common.Util
import ctech.arbitrium.connections.AdbReverseTcpConnection
import ctech.arbitrium.connections.DatagramConnection
import ctech.arbitrium.connections.EmptyConnection
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.jvm.javaType
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    var connection: Connection? = null

    if (args.size == 1) {
        when (args[0]) {
            "authentication" -> return
            "udp" -> connection = DatagramConnection()
            "tcp" -> connection = AdbReverseTcpConnection(getAdbExecPath())
            "generate" -> {
                println(generateClientFunctions())
                exitProcess(0)
            }
        }
    }

    if (connection == null) {
        connection = DatagramConnection()
    }

    val connectionThread = Thread(connection).startThread()

    var hitEsc = false

    while (!hitEsc) {
        if (kbhit() != 0 && getchar() == 27) hitEsc = true
        try {
            Thread.sleep(10)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
    connection.stop()
	connection.close()
    connectionThread.join()
}

public fun getAdbExecPath(): String {
    return when(System.getProperty("os.name")) {
        "Linux" -> "/usr/bin/env adb"
        "Windows" -> "adb"
        else -> ""
    }
}

private external fun kbhit(): Int

private external fun getchar(): Int

/**
 * Method to generate a class containing each of the available functions this server offers
 * Calls [getCallText] for each function
 */
public fun generateClientFunctions(): String {
    val sb = StringBuilder("package ctech.arbitrium")
    sb  .append("\n\n")
        .append("import ctech.arbitrium.common.ReturnTypeHelper")
        .append("\n")
        .append("import kotlinx.coroutines.experimental.Deferred")
        .append("\n\n")
        .append("public class ServerFunctions(private val connection: Connection) {\n")

    val callablesField = Connection::class.java.getDeclaredField("callableFunctions")
    callablesField.isAccessible = true

    @Suppress("UNCHECKED_CAST")
    val funcs = callablesField.get(EmptyConnection()) as HashMap<String, KCallable<*>>
    funcs.values.forEach {
        sb.append(getCallText(it, 1))
    }
    sb.append("}")
    return sb.toString()

}

private fun getCallText(function: KCallable<*>, indentLevel: Short): String { //generate a function
    val sb = StringBuilder()

    val indent = "\t" * indentLevel
    sb.append("\n${indent}public fun ${function.name}(")
    function.parameters.forEachIndexed { i, it ->
        if (it.name == null) return@forEachIndexed
        if (it.isVararg) sb.append("vararg ")
        sb  .append(it.name).append(": ")
        if (!it.isVararg) {
            sb.append((it.type.classifier as KClass<*>).simpleName)
        }
        if (!it.type.arguments.isEmpty()) {
            sb  .append(if (!it.isVararg) "<" else "")
                .append(commaSeparate(it.type.arguments, {
                    (it.type?.classifier as KClass<*>).simpleName!!
                }))
                .append(if (!it.isVararg) ">" else "")

        }
        if (it.type.isMarkedNullable) sb.append("?")

        if (i != function.parameters.size - 1) sb.append(", ")
    }
    sb  .append("): Deferred<${(function.returnType.classifier as KClass<*>).simpleName}?> {\n")
        .append("$indent\treturn connection.callFunction<${(function.returnType.classifier as KClass<*>).simpleName}>(ReturnTypeHelper.")

    if (TypeToken.get(function.returnType.javaType).rawType.isArray) {
        val nameWithArray = "${(function.returnType.classifier as KClass<*>).simpleName}"

        sb.append("getArray")
          .append("(${nameWithArray.replace("Array", "")}")
    } else {
        sb.append("getType")
          .append("(${(function.returnType.classifier as KClass<*>).simpleName}")
    }
    sb.append("::class)")
      .append(", \"${function.name}\"")
      .append(", arrayOf(")

    var prevNull = false
    function.parameters.forEachIndexed { i, it ->
        if (it.name == null) {
            prevNull = true
            return@forEachIndexed
        }
        if (i != 0 && !prevNull) sb.append(", ")
        sb.append(it.name)
        prevNull = false

    }
    sb.append("))")
    sb.append("\n$indent}\n")

    return sb.toString()
}

private inline fun <T> commaSeparate(items: List<T>, getStringValue: (item: T) -> String, delim: String =", "): String {
    val sb = StringBuilder()
    items.forEachIndexed { i, it ->
        if (i != 0) sb.append(delim)
        sb.append(getStringValue(it))
    }
    return sb.toString()
}

operator fun String.times(other: Short): String {
    var res = ""
    for (i in 0 until other) {
        res += this
    }
    return res
}

operator fun String.times(other: Long): String {
    var res = ""
    for (i in 0 until other) {
        res += this
    }
    return res
}

operator fun String.times(other: Int): String {
    var res = ""
    for (i in 0 until other) {
        res += this
    }
    return res
}