package ctech.arbitrium

import com.google.gson.*
import ctech.arbitrium.common.*
import ctech.arbitrium.common.Util.NumberTypeStrings
import ctech.arbitrium.nativemodules.NativeClasses

import ctech.arbitrium.common.sendtypes.*
import kotlinx.coroutines.experimental.launch
import java.net.InetAddress
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.KVisibility
import kotlin.reflect.full.cast
import kotlin.reflect.full.memberFunctions

public abstract class Connection : Runnable {
    protected data class GenericPacket(val data: String, val address: InetAddress, val port: Int)

    private val functions: HashMap<String, (args: Array<ParameterTypePair<*>>?) -> Any?> = HashMap()
    private val callableFunctions: HashMap<String, KCallable<*>> = HashMap()
    private val receiveTypes: HashMap<String, KClass<*>> = HashMap()

    private val defaultCallWithTypePairArgs = { callable: KCallable<*>, params: Array<ParameterTypePair<*>>? ->
        if (params == null) {
            callable.call()
        } else {
            callable.callWithTypePairArgs(*params)
        }
    }

    init {
        //functions
        NativeClasses.nativeProviders.forEach { provider ->
            provider!!::class.memberFunctions
                    .filter { it.visibility == KVisibility.PUBLIC }
                    .filter { //funnily enough, i don't want to expose the functions of [Any]
                        val anyFuncNames: ArrayList<String> = ArrayList()
                        Any::class.memberFunctions.forEach { anyFunc ->
                            anyFuncNames.add(anyFunc.name)
                        }
                        !anyFuncNames.contains(it.name)
                    }
                    .forEach { func ->
                        addFunction(func, { callable, params ->
                            if (params == null) callable.callWithTypePairArgs(
                                ParameterTypePair(provider, TypeHelper.getType(provider::class))
                            ) else callable.callWithTypePairArgs(ParameterTypePair(
                                provider, TypeHelper.getType(provider::class)), *params
                            )
                        })
                        /*callableFunctions[func.name] = func
                        functions[func.name] = { params ->
                            if (params == null) func.callWithTypePairArgs(
                                    ParameterTypePair(provider, TypeHelper.getType(provider::class))
                            ) else func.callWithTypePairArgs(ParameterTypePair(
                                    provider, TypeHelper.getType(provider::class)), *params
                            )
                        }; Unit //everything is fine*/
                    }
        }

        addFunction(::local, defaultCallWithTypePairArgs)
        addFunction(::adb, defaultCallWithTypePairArgs)

        //functions["local"] = { local(it) }
        //functions["adb"] = { adb(it) }

        //return types
        for (kClass in arrayOf(
                Boolean::class,

                Byte::class,
                Char::class,
                Short::class,
                Int::class,
                Long::class,
                Float::class,
                Double::class,

                String::class,

                IntArray::class,
                Array<Int>::class,

                DoubleArray::class,
                Array<Double>::class,

                Array<String>::class)) {
            receiveTypes[kClass.simpleName!!] = kClass
        }

        //aliases


    }

    protected abstract fun setTimeout(timeout: Int)

    private val gson: Gson = GsonBuilder()
            .registerTypeAdapter(ParameterTypePair::class.java, ParameterTypePairDeserializer())
            .create() // create gson and register the custom deserializer

    private val SendTypes = arrayOf(
            FunctionSendType::class,
            ReturnedSendType::class
    )

    private var running: Boolean = true

    private val lock = Any()

    /**
     * Receives data and returns it in String format
     */
    protected abstract fun receive(): GenericPacket

    final override fun run() {
        //setTimeout(3000)
        runLoop@while (running) {
            val data: GenericPacket
            try {
                data = receive()
            } catch (e: Exception) {
                //e.printStackTrace()
                continue@runLoop
            }
            if (data.data.isEmpty()) continue@runLoop
            launch {
                handleMessage(data)
            }
        }
    }

    /**
     * Deconnectionify the connection
     */
    public abstract fun close()

    public final fun stop() {
        running = false
    }

    private fun handleMessage(packet: GenericPacket) {
        val parsedJson = JsonParser().parse(packet.data)

        val jsonAsClass: FunctionSendType? = if (Util.checkHasSignature(parsedJson as JsonObject, FunctionSendType::class)) gson.fromJson<FunctionSendType>(parsedJson, FunctionSendType::class.java) else null


        if (jsonAsClass != null) {
            val res = handleFunction(jsonAsClass)

            println("$jsonAsClass -> $res")
            val toSend: String
            toSend = if (res == null ||
                    jsonAsClass.returnType != TypeHelper.typeToSimpleString(TypeHelper.getType(res::class)) ||
                    !receiveTypes.contains(res::class.simpleName)) {
                gson.toJson(ReturnedSendType<String?>(null, jsonAsClass.uid), TypeHelper.getReturn(String::class))
            }
            //todo: custom returned serialiser

            else gson.toJson(ReturnedSendType(
                    receiveTypes[res::class.simpleName]?.cast(res), jsonAsClass.uid), TypeHelper.getReturn(receiveTypes[res::class.simpleName]!!)
            )
            send(toSend, packet.address, packet.port)
        }
    }

    protected abstract fun send(data: String, address: InetAddress, port: Int)

    private fun handleFunction(call: FunctionSendType): Any? {
        if (call.functionName !in functions.keys) return null
        return functions[call.functionName]!!(call.params)
    }

    fun local(params: Array<String>): String? {
        if (params[0] == SharedConstants.Messages.PING) {
            return SharedConstants.Messages.PONG
        }
        return null
    }

    fun adb(params: Array<String>): Int {
        if (params.isEmpty()) return -1
        var call = ""
        params.forEachIndexed( { i, it -> if (i != 0) call += " "; call += it } )
        return Runtime.getRuntime().exec("${getAdbExecPath()} $call").waitFor()
    }

    private fun KCallable<*>.callWithTypePairArgs(vararg args: ParameterTypePair<*>): Any? {
        try {
            return call(*castParameters(this, args.asList()))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    private fun castParameters(callable: KCallable<*>, args: List<ParameterTypePair<*>>): Array<*> {
        val resArgs = ArrayList<Any>()
        args.forEachIndexed { i, it ->
            if (callable.parameters[i].isVararg || (callable.parameters[i].type.classifier as KClass<*>).java.isArray) {
                resArgs.add(castArray(it))
            } else {
                resArgs.add((callable.parameters[i].type.classifier as KClass<*>).cast(it.parameter))
            }
            Unit
        }
        return resArgs.toTypedArray()
    }

    private fun castArray(argPair: ParameterTypePair<*>): Array<out Any> {
        if (!argPair.type.endsWith("[]")) throw IllegalArgumentException("Specified parameter type pair is not an array")
        val rawArray = argPair.parameter as Array<*>
        val rawType = argPair.type.dropLast(2)

        return when (rawType.toLowerCase()) {
            "byte" -> {
                val resArr = ArrayList<Byte>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as Number).toByte())
                }
                resArr.toTypedArray()
            }
            "char" -> {
                val resArr = ArrayList<Char>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as Number).toChar())
                }
                resArr.toTypedArray()
            }
            "short" -> {
                val resArr = ArrayList<Short>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as Number).toShort())
                }
                resArr.toTypedArray()
            }
            "int", "integer" -> {
                val resArr = ArrayList<Int>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as Number).toInt())
                }
                resArr.toTypedArray()
            }
            "long" -> {
                val resArr = ArrayList<Long>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as Number).toLong())
                }
                resArr.toTypedArray()
            }
            "float" -> {
                val resArr = ArrayList<Float>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as Number).toFloat())
                }
                resArr.toTypedArray()
            }
            "double" -> {
                val resArr = ArrayList<Double>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as Number).toDouble())
                }
                resArr.toTypedArray()
            }
            "boolean" -> {
                val resArr = ArrayList<Boolean>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as Boolean))
                }
                resArr.toTypedArray()
            }
            "string" -> {
                val resArr = ArrayList<String>(rawArray.size)
                rawArray.forEach {
                    resArr.add((it as String))
                }
                resArr.toTypedArray()
            }
            else -> TODO("objects aren't yet implemented")

        }
    }

    private fun addFunction(callable: KCallable<*>, special: ((callable: KCallable<*>, params: Array<ParameterTypePair<*>>?) -> Any?)? = null) {
        callableFunctions[callable.name] = callable
        if (special == null) {
            functions[callable.name] = { callable.call() } //wrapper in lambda for Unit return type
        } else {
            functions[callable.name] = { params -> special(callable, params) }
        }
    }
}
