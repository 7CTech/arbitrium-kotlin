package ctech.arbitrium.nativemodules

abstract class NativeProvider {
    public abstract fun cleanup()
}