package ctech.arbitrium.nativemodules

import ctech.arbitrium.common.CustomSecurityManager

class Admin : NativeProvider() {
    companion object {
        @JvmStatic private val admin: Admin = Admin()
        @JvmStatic fun create(): Admin? {
            return if (CustomSecurityManager().getCallerClassName(2) != "ctech.arbitrium.nativemodules.NativeClasses") null else admin
        }
    }
    public external fun lock()
    //public external fun adbUsb()

    external override fun cleanup()

}