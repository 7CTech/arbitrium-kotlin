package ctech.arbitrium.nativemodules

import ctech.arbitrium.common.CustomSecurityManager
import java.math.BigDecimal
import java.math.RoundingMode

class Stats private constructor(): NativeProvider() {
    companion object {
        @JvmStatic private val stats: Stats = Stats()
        @JvmStatic fun create(): Stats? {
            return if (CustomSecurityManager().getCallerClassName(2) != "ctech.arbitrium.nativemodules.NativeClasses") null else stats
        }
    }

    private var coreCount: Int = 0

    init {
        this.coreCount = getCoreCountInternal()
    }

    public fun getAllCpuUsage(time: Long = 1000000, decimalPoints: Short = -1): DoubleArray {
        val raw = getAllCpuUsage(coreCount, time)
        if (decimalPoints > -1) raw.forEachIndexed { i, it -> raw[i] = BigDecimal(it).setScale(decimalPoints.toInt(), RoundingMode.HALF_UP).toDouble() }
        return raw
    }

    public fun getUsageForCpuId(cpuId: Int, time: Int = 1000000, decimalPoints: Short = -1): Double {
        val raw = getUsageForCpuId(cpuId, coreCount, time)
        return if (decimalPoints > -1) BigDecimal(raw).setScale(decimalPoints.toInt(), RoundingMode.HALF_UP).toDouble() else raw
    }

    public fun getCoreCount(): Int {
        if (coreCount == 0) coreCount = getCoreCountInternal() //idk how the f*ck this will be triggered, but meh
        return coreCount
    }

    private external fun getAllCpuUsage(coreCount: Int, time: Long): DoubleArray
    private external fun getUsageForCpuId(cpuId: Int, coreCount: Int, time: Int): Double
    private external fun getCoreCountInternal(): Int
    public external fun getUsedTotalRamKB(): IntArray

    public external fun hasBattery(): Boolean
    public external fun getBatteryPercentage(): Int
    public external fun isBatteryCharging(): Boolean

    override fun cleanup() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
