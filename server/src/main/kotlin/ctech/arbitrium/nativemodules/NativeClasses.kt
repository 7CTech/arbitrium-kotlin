package ctech.arbitrium.nativemodules

public object NativeClasses {
    init {
        /*try {
            throw Exception()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        System.loadLibrary("ArbitriumNativeClasses")*/
        System.load(System.getProperty("user.dir") + "/" + System.mapLibraryName("ArbitriumNativeClasses"))
    }

    private var stats: Stats? = Stats.create()
    private var admin: Admin? = Admin.create()

    public val nativeProviders: Array<NativeProvider?> = arrayOf(stats, admin)

    @Synchronized
    public fun getStats(): Stats {
        if (stats == null) stats = Stats.create()
        return stats as Stats
    }

    @Synchronized
    public fun getAdmin(): Admin {
        if (admin == null) admin = Admin.create()
        return admin as Admin
    }

    @Synchronized
    public fun cleanup() {
        nativeProviders.forEach { it?.cleanup() }
    }
}