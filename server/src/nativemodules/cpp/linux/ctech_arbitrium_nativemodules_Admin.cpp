//
// Created by ctech on 12/5/17.
//

#include <jni.h>
#include <cstdlib>


#ifdef __cplusplus
extern "C" {
#endif

#include <xdo.h>

static xdo_t *x = xdo_new(":0.0");

JNIEXPORT void JNICALL
Java_ctech_arbitrium_nativemodules_Admin_lock(JNIEnv *env, jobject instance) {
    xdo_send_keysequence_window(x, CURRENTWINDOW, "super+l", 10000);
}

/*JNIEXPORT void JNICALL
Java_ctech_arbitrium_nativemodules_Admin_adbUsb(JNIEnv *env, jobject instance) {
    system("adb usb");
}*/

JNIEXPORT void JNICALL
Java_ctech_arbitrium_nativemodules_Admin_cleanup(JNIEnv *env, jobject instance) {
	xdo_free(x);
}
#ifdef __cplusplus
}
#endif
