//
// Created by ctech on 6/12/17.
//

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

#include <termios.h>
#include <sys/ioctl.h>

JNIEXPORT jint JNICALL
Java_ctech_arbitrium_MainKt_kbhit(JNIEnv *, jclass) {
    static const int STDIN = 0;
    static bool initialized = false;

    if (! initialized) {
        // Use termios to turn off line buffering
        termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, nullptr);
        initialized = true;
    }

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}

JNIEXPORT jint JNICALL
Java_ctech_arbitrium_MainKt_getchar(JNIEnv *, jclass) {
    return getchar();
}

#ifdef __cplusplus
}
#endif
