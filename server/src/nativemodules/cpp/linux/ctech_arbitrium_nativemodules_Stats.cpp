//
// Created by ctech on 4/12/17.
//
#include <jni.h>
#include <fstream>
#include <vector>
#include <unistd.h>
#include <cstring>
#include <iostream>


#ifdef __cplusplus
extern "C" {
#endif
JNIEXPORT jdoubleArray JNICALL
Java_ctech_arbitrium_nativemodules_Stats_getAllCpuUsage(JNIEnv *env, jobject instance, jint coreCount, jlong time) {
    std::ifstream procStatStream;
    procStatStream.open("/proc/stat", std::fstream::in);
    std::vector<std::vector<int>> procStatValues; //Let's not deal with loads of arrays, C++ is nice
    std::vector<double> utilisation;

    for (int i = 0; i < coreCount; i++) {
        char line[100];
        std::string cpuExpected = "cpu";
        cpuExpected.append(1, (char)(i + '0'));
        char cpu[5];
        std::string cpuFancy;
        int user = 0, nice = 0, system = 0, idle = 0, iowait = 0, irq = 0, softirq = 0, steal = 0;
        while (cpuFancy != cpuExpected) {
            procStatStream.getline(line, 100);
            sscanf(line, "%s%d%d%d%d%d%d%d%d", cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal);
            cpuFancy.assign(cpu);
        }
        std::vector<int> procStatValuesL;
        procStatValuesL.push_back(user);
        procStatValuesL.push_back(system);
        procStatValuesL.push_back(nice);
        procStatValuesL.push_back(softirq);
        procStatValuesL.push_back(steal);
        procStatValuesL.push_back(idle);
        procStatValuesL.push_back(iowait);
        procStatValues.push_back(procStatValuesL);
    }
    procStatStream.close();
    usleep((unsigned int)time);
    procStatStream.open("/proc/stat", std::fstream::in);
    for (int i = 0; i < coreCount; i++) {
        char line[100];
        std::string cpuExpected = "cpu";
        cpuExpected.append(1, (char)(i + '0'));
        char cpu[5];
        std::string cpuFancy;
        int user = 0, nice = 0, system = 0, idle = 0, iowait = 0, irq = 0, softirq = 0, steal = 0;
        while (cpuFancy != cpuExpected) {
            procStatStream.getline(line, 100);
            sscanf(line, "%s%d%d%d%d%d%d%d%d", cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal);
            cpuFancy.assign(cpu);
        }
        int activeLast, activeCur, totalLast, totalCur;
        activeLast = procStatValues[i][0] + procStatValues[i][1] + procStatValues[i][2] + procStatValues[i][3] + procStatValues[i][4];
        activeCur = user + system + nice + softirq + steal;
        totalLast = activeLast + procStatValues[i][5] + procStatValues[i][6];
        totalCur = activeCur + idle + iowait;
        utilisation.push_back(100 * ((double)(activeCur - activeLast) / (double)(totalCur - totalLast)));
    }

    procStatStream.close();

    jdoubleArray result = env->NewDoubleArray(coreCount);
    env->SetDoubleArrayRegion(result, 0, coreCount, &utilisation[0]);
    return result;
}

JNIEXPORT jdouble JNICALL
Java_ctech_arbitrium_nativemodules_Stats_getUsageForCpuId(JNIEnv *env, jobject instance, jint cpuId, jint coreCount,
                                                          jint time) {
    if (cpuId > coreCount - 1) return 0.0; //Can't read what doesn't exist
    std::ifstream procStatStream;
    procStatStream.open("/proc/stat", std::fstream::in);

    char line[100];

    std::string cpuExpected = "cpu";
    cpuExpected.append(1, (char)(cpuId + '0'));

    char cpu[5];
    std::string cpuFancy;

    int user = 0, nice = 0, system = 0, idle = 0, iowait = 0, irq = 0, softirq = 0, steal = 0;
    int activeInitial, totalInitial, activeCurrent, totalCurrent;

    while (cpuFancy != cpuExpected) {
        procStatStream.getline(line, 100);
        sscanf(line, "%s%d%d%d%d%d%d%d%d", cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal);
        cpuFancy.assign(cpu);
    }
    cpuFancy.clear();

    activeInitial = user + system + nice + softirq + steal;
    totalInitial = user + system + nice + softirq + steal + idle + iowait;

    procStatStream.close();
    usleep(static_cast<__useconds_t>(time));
    procStatStream.open("/proc/stat", std::fstream::in);

    while (cpuFancy != cpuExpected) {
        procStatStream.getline(line, 100);
        sscanf(line, "%s%d%d%d%d%d%d%d%d", cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal);
        cpuFancy.assign(cpu);
    }

    activeCurrent = user + system + nice + softirq + steal;
    totalCurrent = user + system + nice + softirq + steal + idle + iowait;

    double utilisation = 100 * ((double)(activeCurrent - activeInitial) / (double)(totalCurrent - totalInitial));
    procStatStream.close();
    return utilisation;
}

JNIEXPORT jint JNICALL
Java_ctech_arbitrium_nativemodules_Stats_getCoreCountInternal(JNIEnv *env, jobject instance) {
    std::ifstream procStatStream;
    procStatStream.open("/proc/cpuinfo", std::fstream::in);

    std::string fancyKey;

    char line[100];
    char key[10], dump[10], value[10];

    while (fancyKey != "siblings") {
        procStatStream.getline(line, 100);
        std::sscanf(line, "%[^\t]%[^:]: %[^\n]", key, dump, value);
        fancyKey.assign(key);
    }

    procStatStream.close();
    return (int)std::strtol(value, nullptr, 10);
}

static int getTotalRAMKB() {
    std::ifstream procMemInfoStream;
    procMemInfoStream.open("/proc/meminfo", std::fstream::in);
    char line[120];
    char dump[20];
    char key[20];
    int value = 0;
    std::string keyFancy;
    while (keyFancy != "MemTotal") {
        procMemInfoStream.getline(line, 120);
        sscanf(line, "%[^:]%s%d", key, dump, &value);
        keyFancy.assign(key);
    }
    procMemInfoStream.close();
    return value;
}

static int getAvailableRAMKB() {
    std::ifstream procMemInfoStream;
    procMemInfoStream.open("/proc/meminfo", std::fstream::in);
    char line[120];
    char dump[20];
    char key[20];
    int value = 0;
    std::string keyFancy;
    while (keyFancy != "MemAvailable") {
        procMemInfoStream.getline(line, 120);
        sscanf(line, "%[^:]%s%d", key, dump, &value);
        keyFancy.assign(key);
    }
    procMemInfoStream.close();
    return value;
}

static int getUsedRAMKB() {
    return getTotalRAMKB()- getAvailableRAMKB();
}

JNIEXPORT jintArray JNICALL
Java_ctech_arbitrium_nativemodules_Stats_getUsedTotalRamKB(JNIEnv *env, jobject instance) {
    jintArray result = env->NewIntArray(2);
    jint outputs[2] = {getUsedRAMKB(), getTotalRAMKB()};
    env->SetIntArrayRegion(result, 0, 2, outputs);
	return result;
}

JNIEXPORT jboolean JNICALL
Java_ctech_arbitrium_nativemodules_Stats_hasBattery(JNIEnv *env, jobject instance) {
    if (system("ls /sys/class/power_supply | grep \"BAT\" > /dev/null") != 0) return static_cast<jboolean>(false);
    std::ifstream batStream;
    batStream.open("/sys/class/power_supply/BAT0/present");
    char line[120];
    batStream.getline(line, 120);
    batStream.close();
    return (jboolean)strtol(line, nullptr, 10);
}

JNIEXPORT jint JNICALL
Java_ctech_arbitrium_nativemodules_Stats_getBatteryPercentage(JNIEnv *env, jobject instance) {
    if (system("ls /sys/class/power_supply | grep \"BAT\" > /dev/null") != 0) return -1;
    std::ifstream batStream;
    char line[120];
    batStream.open("/sys/class/power_supply/BAT0/capacity");
    batStream.getline(line, 120);
    return (jshort)(strtol(line, nullptr, 10));
}

JNIEXPORT jboolean JNICALL
Java_ctech_arbitrium_nativemodules_Stats_isBatteryCharging(JNIEnv *env, jobject instance) {
    if (system("ls /sys/class/power_supply | grep \"BAT\" > /dev/null") != 0) return static_cast<jboolean>(false);
    std::ifstream batStream;
    char line[120];
    batStream.open("/sys/class/power_supply/BAT0/status");
    batStream.getline(line, 120);
    return static_cast<jboolean>(strcmp(line, "Charging") == 0);
}

#ifdef __cplusplus
}
#endif
