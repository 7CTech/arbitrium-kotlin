package ctech.arbitrium.connections

import ctech.arbitrium.Connection
import ctech.arbitrium.common.SharedConstants
import kotlinx.coroutines.experimental.launch
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.Socket
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by ctech on 1/01/18 in Arbitrium
 */
class AdbReverseTcpConnection : Connection("127.0.0.1") {
    private var socket: Socket? = null
    private val socketInitialised = AtomicBoolean(false)

    private val lock = Object()

    init {
        launch {
            try {
                socket = Socket(address, SharedConstants.PORT)
            } catch (e: Exception) {
                failed = true
            }
            socketInitialised.set(true)
            synchronized(lock) { lock.notifyAll() }
        }
    }

    override fun setTimeout(timeout: Int) {
        socket?.soTimeout = timeout
    }

    override fun getTimeout(): Int {
        return try {
            socket?.soTimeout ?: 0
        } catch (e: Exception) {
            0
        }
    }

    override fun isClosed(): Boolean {
        return socket?.isClosed ?: true
    }

    override fun send0(data: String) {
        println("sending: $data")
        socket?.outputStream?.write("$data\n".toByteArray())
    }

    override fun receive(): String {
        if (!socketInitialised.get()) synchronized(lock) { lock.wait() }
        return try {
            BufferedReader(InputStreamReader(socket?.inputStream)).readLine()
        } catch (e: Exception) {
            ""
        }
    }

    override fun close0() {
        socket?.close()
    }
}