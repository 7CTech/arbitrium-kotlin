package ctech.arbitrium.fragments

import android.os.Bundle
import android.support.v4.app.Fragment

/**
 * Created by ctech on 2/01/18 in Arbitrium
 */
class UsageDetailsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    companion object {
        private val TAG = "Arbitrium:Usage"
    }
}