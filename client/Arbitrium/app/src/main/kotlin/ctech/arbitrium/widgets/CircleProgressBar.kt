package ctech.arbitrium.widgets

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

import ctech.arbitrium.R

/**
 * https://github.com/Pedramrn/CircularProgressBar
 */

class CircleProgressBar(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    private var strokeWidth = 4f
    private var progress = 0f
    private var min = 0
    private var max = 100


    private val startAngle = -90
    private var backgroundColor = Color.LTGRAY
    private var foregroundColor = Color.MAGENTA
    private var rectF: RectF? = null
    private lateinit var backgroundPaint: Paint
    private lateinit var foregroundPaint: Paint

    init {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        rectF = RectF()
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.CircleProgressBar, 0, 0)
        try {
            strokeWidth = typedArray.getDimension(R.styleable.CircleProgressBar_progressBarThickness, strokeWidth)
            progress = typedArray.getFloat(R.styleable.CircleProgressBar_progress, progress)
            foregroundColor = typedArray.getInt(R.styleable.CircleProgressBar_progressBarForegroundColor, foregroundColor)
            backgroundColor = typedArray.getInt(R.styleable.CircleProgressBar_progressBarBackgroundColor, backgroundColor)
            min = typedArray.getInt(R.styleable.CircleProgressBar_min, min)
            max = typedArray.getInt(R.styleable.CircleProgressBar_max, max)
        } finally {
            typedArray.recycle()
        }

        updatePaint()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = View.getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val width = View.getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val min = Math.min(width, height)
        setMeasuredDimension(min, min)
        rectF!!.set(0 + strokeWidth / 2, 0 + strokeWidth / 2, min - strokeWidth / 2, min - strokeWidth / 2)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawOval(rectF!!, backgroundPaint)
        val angle = 360 * progress / max
        canvas.drawArc(rectF!!, startAngle.toFloat(), angle, false, foregroundPaint)
    }

    fun setProgress(progress: Float) {
        this.progress = progress
        invalidate() //Redraw
    }

    fun setStrokeWidth(strokeWidth: Int) {
        this.strokeWidth = strokeWidth.toFloat()
        updatePaint()
    }

    override fun setBackgroundColor(color: Int) {
        this.backgroundColor = color
        updatePaint()
    }

    fun setForegroundColor(color: Int) {
        this.foregroundColor = color
        updatePaint()
    }

    private fun updatePaint() {
        backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        backgroundPaint.color = backgroundColor
        backgroundPaint.style = Paint.Style.STROKE
        backgroundPaint.strokeWidth = strokeWidth

        foregroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        foregroundPaint.color = foregroundColor
        foregroundPaint.style = Paint.Style.STROKE
        foregroundPaint.strokeWidth = strokeWidth
        invalidate()
    }

}
