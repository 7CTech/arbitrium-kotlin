package ctech.arbitrium

/**
 * Created by ctech on 8/8/17.
 */

object Constants {
    var PAGE_COUNT = 2
    const val PREFS = "ArbitriumPrefs"

    object Prefs {
        const val IP_DIALOG = "ipDialog"
    }
}
