package ctech.arbitrium

import ctech.arbitrium.fragments.ButtonsFragment

/**
 * Used to store a Buttons information for the ButtonFragment
 * It allows a simple array to be created rather than tracking multiple
 * @see ButtonsFragment
 */
data class ButtonDetails
/**
 * @param text button text
 * @param onClick method to call on click
 * @param group button tab
 */
(
        /**
         * Button text
         */
        var text: String,
        /**
         * Method to call upon clicking the button
         */
        var onClick: () -> Unit,
        /**
         * Tab the button belongs in
         *
         * @see ButtonsFragment.ButtonGroups
         */
        var group: ButtonsFragment.ButtonGroups)
