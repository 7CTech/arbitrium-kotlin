package ctech.arbitrium


import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import ctech.arbitrium.common.*

import ctech.arbitrium.common.sendtypes.FunctionSendType
import ctech.arbitrium.common.sendtypes.ReturnedSendType
import java.net.Inet4Address
import java.net.InetAddress
import ctech.arbitrium.common.Extensions.then
import ctech.arbitrium.common.Extensions.containsNulls
import kotlinx.coroutines.experimental.*
import java.lang.reflect.Type
import java.net.SocketException
import java.util.*
import java.util.concurrent.TimeoutException

/**
* Created by ctech on 12/6/17 in Arbitrium
*/
public abstract class Connection(ip: String) : Runnable {
    protected val address: Inet4Address = InetAddress.getByName(ip) as Inet4Address
    protected var closed = false
    protected val receivedVals: DelayedMap<String, String> = DelayedMap()

    private val gson: Gson = Gson()

    protected abstract fun setTimeout(timeout: Int)
    protected abstract fun getTimeout(): Int

    protected var failed = false

    public var serverFunctions: ServerFunctions = ServerFunctions(this)

    public abstract fun isClosed(): Boolean

    private final fun getFunctionSendType(returnType: Type, functionName: String, params: Array<out Any?>): FunctionSendType {
        val paramsToPass: ArrayList<ParameterTypePair<*>>? = ArrayList()
        if (!params.containsNulls()) params.forEach { paramsToPass?.add(ParameterTypePair(it!!, TypeHelper.getType(it::class))); Unit }
        return FunctionSendType(functionName, paramsToPass?.toTypedArray(), returnType)
    }

    public final fun <T : Any> callFunction(returnType: Type, functionName: String, params: Array<out Any?> = arrayOf(), attemptCount: Int = 3, timeout: Long = 2000): Deferred<T?> {
        val function = getFunctionSendType(returnType, functionName, params)
        return send<T>(gson.toJson(function), attemptCount, timeout, returnType, function.uid)
    }

    public final fun <T : Any> callFunctionWithCallback(returnType: Type, functionName: String, vararg params: Any = arrayOf(), attemptCount: Int = 3, timeout: Long = 2000, fulfilled: (T?) -> Unit = {}, rejected: (Throwable) -> Unit = {}) {
        val function = getFunctionSendType(returnType, functionName, params)
        send<T>(gson.toJson(function), attemptCount, timeout, returnType, function.uid).then(fulfilled, rejected)
    }

    protected abstract fun send0(data: String)
    protected abstract fun receive(): String

    private final fun <T> send(data: String, attemptCount: Int, timeout: Long, type: Type, uid: String): Deferred<T?> = async {
        if (attemptCount < 1) throw IllegalStateException("caller doesn't want to attempt to receive? something ain't right...")
        try {
            var received = ""
            for (i in 0 until attemptCount) {
                try {
                    send0(data)
                    received = receivedVals.getAndWait(uid, timeout)
                    break
                } catch (e: TimeoutException) {
                    Log.w(TAG, "Timed out receiving data... try ${i + 1} of $attemptCount")
                }
            }
            if (received.isEmpty() || received == "") throw Exception("no data")
            println("$type")
            val returned = gson.fromJson<ReturnedSendType<T>>(received, type)
            returned.returned
        } catch(e: Exception) {
            null
        }
    }

    public abstract fun close0()

    //public abstract fun reconnect(): Boolean

    public final fun close() {
        closed = true
        close0()
    }

    public final override fun run() {
        while (!closed) {
            val received = try {
                receive()
            } catch (e: SocketException) {
                return@run
            }
            println(received)
            if (received == "") continue
            receivedVals.put(JsonParser().parse(received).asJsonObject["uid"].asString, received)
        }
    }

    public final suspend fun checkConnection(): Boolean {
        return callFunction<String>(
                TypeHelper.getReturn(String::class), SharedConstants.LOCAL_FUN, arrayOf(arrayOf(SharedConstants.Messages.PING)),
                1, 200
        ).await() == SharedConstants.Messages.PONG
    }

    public final fun checkConnection(callback: suspend (Boolean) -> Unit = {}) = callFunction<String>(
            TypeHelper.getReturn(String::class), SharedConstants.LOCAL_FUN, arrayOf(arrayOf(SharedConstants.Messages.PING)),
            1, 200

    ).then( { launch { callback(it == SharedConstants.Messages.PONG) } }, { launch { callback(false) } } )

    public final fun killServer(callback: suspend (Boolean) -> Unit = {}) = callFunction<Boolean>(
            TypeHelper.getReturn(Boolean::class), SharedConstants.LOCAL_FUN, arrayOf(arrayOf(SharedConstants.Messages.KILL)),
            1, 50

    ).then( { launch { callback(it == true) } }, { launch { callback(false) } } )

    final companion object {
        private const val TAG = "Arbitrium:Connection"
        //private set
    }

    final override fun toString(): String {
        return "<${this::class.simpleName}>${address.hostName}:${SharedConstants.PORT}"
    }
}