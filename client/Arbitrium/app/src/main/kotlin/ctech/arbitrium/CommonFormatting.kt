package ctech.arbitrium

import android.text.Html
import android.text.Spanned
import android.text.SpannedString
import java.text.DecimalFormat

/**
 * Contains methods used for formatting Strings and numbers
 */
object CommonFormatting {
    /**
     * Generates a String for displaying about RAM info
     * Result looks something like
     * `1.0GB/4.0GB (25%)`
     * @param usage currently used RAM in GB
     * @param total total RAM in GB
     * @return formatted String
     */
    fun formatRAM(usage: Double, total: Double): String {
        return "${DecimalFormat("00.0").format(usage)}GB/${DecimalFormat("#0.0").format(total)}GB"
    }

    /**
     * Generates the String for displaying CPU Usage
     * @param usage CPU usage as a percent
     * @return formatted String
     */
    fun formatCpuText(usage: Double): String {
        return "${Integer.toString(usage.toInt())} %"
    }

    /**
     * Converts Kilobytes to Gigabytes
     * @param kb KB
     * @return KB in GB
     */
    fun KBToGB(kb: Int): Double {
        return kb.toDouble()/1024.0/1024.0 //KB -> MB -> GB
    }
}
