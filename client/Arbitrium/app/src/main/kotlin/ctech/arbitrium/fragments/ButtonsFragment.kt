package ctech.arbitrium.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridLayout
import android.widget.TabHost

import java.util.ArrayList

import ctech.arbitrium.ButtonDetails
import ctech.arbitrium.R



/**
 * This fragment is designed to hold the page of buttonDetails.
 * It supports a TabHost which allows for different pages internal to it
 * @see TabHost
 */
class ButtonsFragment : Fragment() {

    /**
     * View created by the Fragment
     */
    private lateinit var mView: View

    /**
     * Array to store all the buttonDetails. The buttonDetails are added before the mView is created.
     * This means that they have to be stored internally, then be populated.
     */
    private var buttonDetails: ArrayList<ButtonDetails> = ArrayList()

    /**
     * Used for grouping buttonDetails into tabs
     */
    enum class ButtonGroups private constructor(
            /**
             * Fancy name
             */
            private val s: String) {
        ADMIN("admin"),
        UTIL("util");

        override fun toString(): String {
            return s
        }
    }

    /**
     * Method to create the mView.
     * This retreives the UI from XML, as well as populates and creates the buttonDetails.
     * @param inflater
     * @param container
     * @param savedInstanceState bundle with args
     * @return mView created by the fragment
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_buttons, container, false)

        val tabHostButton = mView.findViewById<TabHost>(R.id.buttonTabHost)

        tabHostButton.setup()

        val utilTab = tabHostButton.newTabSpec("UtilTab")
                .setIndicator("Util")
                .setContent(R.id.utilGrid)
        tabHostButton.addTab(utilTab)

        val adminTab = tabHostButton.newTabSpec("AdminTab")
                .setIndicator("Admin")
                .setContent(R.id.adminGrid)
        tabHostButton.addTab(adminTab)

        for (i in 0 until buttonDetails.size) {
            val buttonDetails = buttonDetails[i]
            var layoutGroup: GridLayout

            layoutGroup = when (buttonDetails.group) {
                ButtonGroups.ADMIN -> mView.findViewById(R.id.adminGrid)
                ButtonGroups.UTIL -> mView.findViewById(R.id.utilGrid)
            }

            layoutGroup.rowCount = 4
            layoutGroup.columnCount = 4
            val button = Button(layoutGroup.context)

            button.text = buttonDetails.text
            button.gravity = Gravity.CENTER
            button.layoutParams = ViewGroup.LayoutParams(resources.getDimension(R.dimen.buttonWidth).toInt(), resources.getDimension(R.dimen.buttonHeight).toInt())
            button.setOnClickListener ({
                try {
                    buttonDetails.onClick()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })

            val pos = getNextLayoutPos(layoutGroup)

            layoutGroup.addView(button, GridLayout.LayoutParams(GridLayout.spec(pos[1]), GridLayout.spec(pos[0])))

            mView.invalidate()
            mView.requestLayout()
        }
        return mView
    }

    /**
     * Adds a button to the fragment
     * @param buttonDetails button to add
     */
    fun addButton(buttonDetails: ButtonDetails) {
        this.buttonDetails.add(buttonDetails)
    }

    /**
     * Adds a button to the fragment
     * @param text button text
     * @param onClickMethod button method to run on click
     * @param group button group
     *
     * @see ButtonDetails
     */
    fun addButton(text: String, onClickMethod: () -> Unit, group: ButtonGroups) {
        buttonDetails.add(ButtonDetails(text, onClickMethod, group))
    }

    /**
     * Internal method used for spacing out GridLayout items
     * @param layout grid layout which is having an item added
     * @return array with the format [col, row] of where to put the next item
     *
     * @see GridLayout
     */
    private fun getNextLayoutPos(layout: GridLayout): IntArray {
        return IntArray(2, { if (it == 0) layout.childCount % layout.columnCount else layout.childCount / layout.rowCount } )
    }

    companion object {
        /**
         * Logging Tag
         * @see android.util.Log
         */
        private val TAG = "Arbitrium:Buttons"

        /**
         * Helper to create a new instance of the fragment
         * @return a new instance of the fragment
         */
        fun newInstance(): ButtonsFragment {
            val buttonsFragment = ButtonsFragment()
            buttonsFragment.arguments = Bundle()
            return buttonsFragment
        }
    }
}
