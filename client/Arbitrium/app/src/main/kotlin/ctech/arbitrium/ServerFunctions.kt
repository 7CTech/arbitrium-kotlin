package ctech.arbitrium

import ctech.arbitrium.common.ReturnTypeHelper
import kotlinx.coroutines.experimental.Deferred

public class ServerFunctions(private val connection: Connection) {

	public fun getBatteryPercentage(): Deferred<Int?> {
		return connection.callFunction<Int>(ReturnTypeHelper.getType(Int::class), "getBatteryPercentage", arrayOf())
	}

	public fun getCoreCount(): Deferred<Int?> {
		return connection.callFunction<Int>(ReturnTypeHelper.getType(Int::class), "getCoreCount", arrayOf())
	}

	public fun isBatteryCharging(): Deferred<Boolean?> {
		return connection.callFunction<Boolean>(ReturnTypeHelper.getType(Boolean::class), "isBatteryCharging", arrayOf())
	}

	public fun getUsageForCpuId(cpuId: Int, time: Int, decimalPoints: Short): Deferred<Double?> {
		return connection.callFunction<Double>(ReturnTypeHelper.getType(Double::class), "getUsageForCpuId", arrayOf(cpuId, time, decimalPoints))
	}

	public fun cleanup(): Deferred<Unit?> {
		return connection.callFunction<Unit>(ReturnTypeHelper.getType(Unit::class), "cleanup", arrayOf())
	}

	public fun hasBattery(): Deferred<Boolean?> {
		return connection.callFunction<Boolean>(ReturnTypeHelper.getType(Boolean::class), "hasBattery", arrayOf())
	}

	public fun getUsedTotalRamKB(): Deferred<IntArray?> {
		return connection.callFunction<IntArray>(ReturnTypeHelper.getArray(Int::class), "getUsedTotalRamKB", arrayOf())
	}

	public fun lock(): Deferred<Unit?> {
		return connection.callFunction<Unit>(ReturnTypeHelper.getType(Unit::class), "lock", arrayOf())
	}

	public fun local(vararg params: Any): Deferred<String?> {
		return connection.callFunction<String>(ReturnTypeHelper.getType(String::class), "local", arrayOf(params))
	}

	public fun adb(vararg params: String): Deferred<Int?> {
		return connection.callFunction<Int>(ReturnTypeHelper.getType(Int::class), "adb", arrayOf(params))
	}

	public fun getAllCpuUsage(time: Long, decimalPoints: Short): Deferred<DoubleArray?> {
		return connection.callFunction<DoubleArray>(ReturnTypeHelper.getArray(Double::class), "getAllCpuUsage", arrayOf(time, decimalPoints))
	}
}
