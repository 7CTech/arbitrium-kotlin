package ctech.arbitrium

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.os.Looper
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.widget.EditText
//import ctech.arbitrium.R.id.viewPager
import ctech.arbitrium.common.LateReturnContainer
import ctech.arbitrium.common.ReturnTypeHelper
import ctech.arbitrium.connections.AdbReverseTcpConnection
import ctech.arbitrium.connections.DatagramConnection
import ctech.arbitrium.fragments.ButtonsFragment
import ctech.arbitrium.fragments.GraphsFragment
import kotlinx.coroutines.experimental.launch
import kotlinx.android.synthetic.main.activity_main.*

//typealias RT = ReturnTypeHelper

public class MainActivity : AppCompatActivity() {
    private var cpuCores: Int = 0
    private lateinit var adapter: MainPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main) //Initiliases all UI elements
        val ipContainer: LateReturnContainer<String> = LateReturnContainer()
        val dialog = createInputDialog("Server IP:", "OK", "Cancel", getSharedPreferences(Constants.PREFS, 0).getString(Constants.Prefs.IP_DIALOG, ""), ipContainer)

        val localIPs = arrayOf("127.0.0.1", "127.00.00.1")

        //connection = AdbReverseTcpConnection()
        //connection = DatagramConnection("192.168.0.16")
        //Thread(connection).start()

        val connectionFunc: suspend (connected: Boolean) -> Unit = {
            Looper.prepare()
            if (it) {
                cpuCores = serverFunctions.getCoreCount().await()!!

                println("core count: $cpuCores")

                adapter = MainPagerAdapter(supportFragmentManager, cpuCores, connection!!/*, applicationContext*/)
                runOnUiThread {
                    viewPager.adapter = adapter
                    viewPager.currentItem = 0
                }

                //runOnUiThread { circleIndicator.setViewPager(viewPager) }
            } else {
                runOnUiThread {
                    createDialog("Not Connected", "Failed to connect to the server", null, null, ButtonData("Quit App"), this)
                            .setOnDismissListener { dialog ->
                                dialog.dismiss()
                                killApp(1)
                            }
                }
            }
        }


        dialog.setOnDismissListener {
            if (!ipContainer.success) return@setOnDismissListener
            println(ipContainer.returned)
            val settings = getSharedPreferences(Constants.PREFS, 0)
            settings.edit().putString(Constants.Prefs.IP_DIALOG, ipContainer.returned).apply()
            connection = if (ipContainer.returned in localIPs) AdbReverseTcpConnection() else DatagramConnection(ipContainer.returned)
            serverFunctions = connection!!.serverFunctions
            Thread(connection).start()
            connection?.checkConnection { connectionFunc(it) }
        }
        dialog.setOnCancelListener {
            launch { connectionFunc(false) }
        }
    }



    /**
     * Dodgy Function to create an input dialog
     * @param title Dialog title
     * @param posButton Positive button text
     * @param negButton Negative button text
     * @param res Return value of the input dialog
     */
    private fun createInputDialog(title: String, posButton: String, negButton: String, defaultText: String, res: LateReturnContainer<String>): AlertDialog {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT
        input.setText(defaultText)
        builder.setView(input)
        builder.setPositiveButton(posButton, { dialog, which ->
            res.returned = input.text.toString()
            dialog.dismiss()
        })
        builder.setNegativeButton(negButton, { dialog, which ->
            res.returned = ""
            res.success = false
            dialog.cancel()
        })


        return builder.show()
    }


    override fun onDestroy() {
        connection?.close()
        super.onDestroy()
    }

    /**
     * View Pager - allows the use of multiple pages within the app
     * @see FragmentPagerAdapter
     */
    class MainPagerAdapter
    /**
     * Used to setup each fragment and their necessary variables within the adapter
     * @param fm fragment manager required for the pager adapter
     * @param coreCount number of cpu cores
     */
    (fm: FragmentManager, private val coreCount: Int, private val connection: Connection/*, private val appContext: Context*/) : FragmentPagerAdapter(fm) {
        private val TAG = "Arbitrium:ViewPager"
        private var graphsFragment: GraphsFragment = GraphsFragment.newInstance(coreCount)
        private var buttonsFragment: ButtonsFragment = ButtonsFragment.newInstance()

        init {
            addButtons()
            //setContext()
        }

        /**
         * Gets the view at the specified position
         * @param position the position to get the view of
         * @return the view at the specified position
         */
        override fun getItem(position: Int): Fragment? {
            Log.d(TAG, "position: $position")
            return when (position) {
                0 -> graphsFragment
                1 -> buttonsFragment
                else -> null
            }
        }

        /**
         * @return number of pages
         */
        override fun getCount(): Int {
            return Constants.PAGE_COUNT
        }

        private fun addButtons() {
            buttonsFragment.addButton("ADB USB", {
                serverFunctions.adb("usb")
            }, ButtonsFragment.ButtonGroups.UTIL)

            buttonsFragment.addButton("Lock", {
                serverFunctions.lock()
            }, ButtonsFragment.ButtonGroups.ADMIN)

            buttonsFragment.addButton("Kill Server", {
                connection.killServer()
                connection.close()
                try {
                    Thread.sleep(20)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                killApp(1)
            }, ButtonsFragment.ButtonGroups.UTIL)

            buttonsFragment.addButton("Remove Reverse", {
                serverFunctions.adb("reverse", "--remove-all")
            }, ButtonsFragment.ButtonGroups.ADMIN)
        }

        /*private fun setContext() {
            graphsFragment.setAppContext(appContext)
        }*/
    }

    companion object {
        private val TAG = "Arbitrium:Main"
        public val timeOffset = System.currentTimeMillis()
        public lateinit var serverFunctions: ServerFunctions
        private var connection: Connection? = null

        /**
         * Helper to check if [connection] is closed
         * @see [Connection.isClosed]
         */
        public fun isConnectionClosed(): Boolean {
            return connection == null || connection!!.isClosed()
        }

        /**
         * Used to gracefully quit and relaunch the app
         */
        public fun restartApp(context: Context) {
            println("restarting")
            /*connection?.close()
            val startActivity = Intent(context, MainActivity::class.java)
            val mPendingIntent = PendingIntent.getActivity(context, 1, startActivity, PendingIntent.FLAG_CANCEL_CURRENT)
            (context.getSystemService(Context.ALARM_SERVICE) as AlarmManager)
                    .set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
            System.exit(2)*/
        }

        public fun killApp(status: Int) {
            connection?.close()
            System.exit(status)
        }

        /**
         * Function to quickly make a dialog
         * @param title Dialog title
         * @param text Dialog text
         * @param button Dialog button text
         * @return the dialog
         */
        public fun createDialog(title: String, text: String, negButton: ButtonData?, posButton: ButtonData?,
                                 neutralButton: ButtonData?, context: Context): AlertDialog {
            val dialog = AlertDialog.Builder(context).create()
            dialog.setTitle(title)
            dialog.setMessage(text)
            if (negButton != null) dialog.setButton(AlertDialog.BUTTON_NEGATIVE, negButton.name, negButton.listener)
            if (posButton != null) dialog.setButton(AlertDialog.BUTTON_NEGATIVE, posButton.name, posButton.listener)
            if (neutralButton != null) dialog.setButton(AlertDialog.BUTTON_NEGATIVE, neutralButton.name, neutralButton.listener)
            dialog.show()
            return dialog
        }
    }

    data class ButtonData(val name: String, val listener: (dialog: DialogInterface, which: Int) -> Unit = { it, _ -> it.dismiss() })
}