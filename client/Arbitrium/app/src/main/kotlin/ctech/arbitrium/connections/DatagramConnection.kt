package ctech.arbitrium.connections


import ctech.arbitrium.Connection
import ctech.arbitrium.common.SharedConstants

import java.net.DatagramSocket
import java.net.DatagramPacket

/**
* Created by ctech on 12/6/17 in Arbitrium
*/
class DatagramConnection(ip: String) : Connection(ip) {
    private val socket = DatagramSocket(SharedConstants.PORT)

    override fun setTimeout(timeout: Int) {
        socket.soTimeout = timeout
    }

    override fun getTimeout(): Int {
        return socket.soTimeout
    }

    override fun send0(data: String) {
        socket.send(DatagramPacket(data.toByteArray(), data.length, address, SharedConstants.PORT))
    }

    override fun receive(): String {
        val packet = DatagramPacket(ByteArray(SharedConstants.BUF_LEN), SharedConstants.BUF_LEN)
        socket.receive(packet)
        return String(packet.data, 0, packet.length)
    }

    override fun close0() {
        socket.disconnect()
        socket.close()
    }

    override fun isClosed(): Boolean {
        return socket.isClosed
    }

    companion object {
        private val TAG = "Arbitrium:Datagram"
    }
}