package ctech.arbitrium.fragments

import android.content.Context
import android.content.res.Configuration
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.text.Html
import android.text.InputType
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.ProgressBar
import android.widget.TextView
import ctech.arbitrium.CommonFormatting
import ctech.arbitrium.MainActivity
import ctech.arbitrium.R
import ctech.arbitrium.ServerFunctions
import ctech.arbitrium.common.Extensions.startThread
import ctech.arbitrium.common.ReturnTypeHelper
import ctech.arbitrium.widgets.CircleProgressBar
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.runBlocking
import java.text.DecimalFormat

class GraphsFragment : Fragment() {
    private var coreCount: Int = 0
    private lateinit var coreDetailsArray: ArrayList<TextView>
    private lateinit var coreProgressArray: ArrayList<ProgressBar>

    private lateinit var mView: View

    private lateinit var ramProgress: CircleProgressBar
    private lateinit var percentText: TextView
    private lateinit var ramDetails: TextView

    private lateinit var updateThread: Thread

    //private lateinit var appContext: Context


    override fun onCreate(savedInstance: Bundle?) {
        super.onCreate(savedInstance)
        coreCount = arguments.getInt("cpuCores", 0)
        coreDetailsArray = ArrayList(coreCount)
        coreProgressArray = ArrayList(coreCount)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_graphs, container, false)
        populateUI()
        initAndStartUpdating()
        return mView
    }

    /*public fun setAppContext(appContext: Context) {
        this.appContext = appContext
    }*/

    //UI
    private fun populateUI() {
        val gridCpu = mView.findViewById<GridLayout>(R.id.cpuGridLayout)
        val constraintRam = mView.findViewById<ConstraintLayout>(R.id.ramConstraintLayout)
        //region CPU

        gridCpu.columnCount = 2 //if this changes there are problems
        gridCpu.rowCount = coreCount

        for (i in 0 until coreCount) {
            val coreDetails = TextView(gridCpu.context)
            coreDetails.text = "0%"
            coreDetails.setPaddingRelative(0, 0, resources.getDimension(R.dimen.cpuTextPaddingEnd).toInt(), 0)
            coreDetails.gravity = Gravity.END
            coreDetails.visibility = View.VISIBLE

            val detailsParams = GridLayout.LayoutParams()
            detailsParams.height = resources.getDimension(R.dimen.cpuTextHeight).toInt()
            detailsParams.width = resources.getDimension(R.dimen.cpuTextWidth).toInt()
            detailsParams.setGravity(Gravity.CENTER)
            detailsParams.columnSpec = GridLayout.spec(0)
            detailsParams.rowSpec = GridLayout.spec(i)

            gridCpu.addView(coreDetails, detailsParams)
            coreDetailsArray.add(coreDetails)

            val coreProgressBar = ProgressBar(gridCpu.context, null, android.R.attr.progressBarStyleHorizontal)
            coreProgressBar.progress = 0
            coreProgressBar.visibility = View.VISIBLE

            val barParams = GridLayout.LayoutParams()
            barParams.height = resources.getDimension(R.dimen.cpuBarHeight).toInt()
            barParams.width = resources.getDimension(R.dimen.cpuBarWidth).toInt()
            barParams.setGravity(Gravity.CENTER)
            barParams.columnSpec = GridLayout.spec(1)
            barParams.rowSpec = GridLayout.spec(i)

            gridCpu.addView(coreProgressBar, barParams)
            coreProgressArray.add(coreProgressBar)

        }

        gridCpu.invalidate()
        //endregion

        //region RAM
        ramProgress = CircleProgressBar(constraintRam.context, null)
        ramProgress.setBackgroundColor(resources.getColor(R.color.colorPlaceholder))
        ramProgress.setForegroundColor(resources.getColor(R.color.colorAccent))
        ramProgress.setStrokeWidth(resources.getDimension(R.dimen.ramUsageThickness).toInt())
        ramProgress.id = R.id.ramProgress
        constraintRam.addView(ramProgress)

        percentText = TextView(constraintRam.context)
        percentText.gravity = Gravity.CENTER
        percentText.inputType = InputType.TYPE_NULL
        percentText.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.ramTextSize))
        percentText.setTypeface(null, Typeface.BOLD)
        percentText.backgroundTintList = resources.getColorStateList(R.color.transparent)
        percentText.text = "0.0%"
        percentText.id = R.id.ramText
        constraintRam.addView(percentText)

        ramDetails = TextView(constraintRam.context)
        ramDetails.gravity = Gravity.CENTER
        ramDetails.setText(R.string.ramUsages)
        ramDetails.id = R.id.ramDetails
        constraintRam.addView(ramDetails)
        //endregion

        generateCPUConstraintSet(resources.configuration.orientation).applyTo(mView.findViewById<ConstraintLayout>(R.id.graphsConstraintLayout))
        generateRAMConstraintSet(resources.configuration.orientation).applyTo(constraintRam)
    }

    private fun generateCPUConstraintSet(orientation: Int): ConstraintSet {
        val gridCpu = mView.findViewById<GridLayout>(R.id.cpuGridLayout)
        val constraintRam = mView.findViewById<ConstraintLayout>(R.id.ramConstraintLayout)
        val constraintSet = ConstraintSet()
        //region RAM
        constraintSet.constrainWidth(R.id.ramConstraintLayout, resources.getDimension(R.dimen.ramConstraintWidth).toInt())
        constraintSet.constrainHeight(R.id.ramConstraintLayout, resources.getDimension(R.dimen.ramConstraintHeight).toInt())


        constraintSet.setMargin(R.id.ramConstraintLayout, ConstraintSet.BOTTOM, resources.getDimension(R.dimen.ramConstraintMarginBottom).toInt())
        constraintSet.setMargin(R.id.ramConstraintLayout, ConstraintSet.END, resources.getDimension(R.dimen.ramConstraintMarginEnd).toInt())

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            constraintSet.setMargin(R.id.ramConstraintLayout, ConstraintSet.TOP, resources.getDimension(R.dimen.ramConstraintMarginTop).toInt())
            constraintSet.connect(R.id.ramConstraintLayout, ConstraintSet.TOP, (constraintRam.parent as View).id, ConstraintSet.TOP)
            constraintSet.connect(R.id.ramConstraintLayout, ConstraintSet.BOTTOM, (constraintRam.parent as View).id, ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.ramConstraintLayout, ConstraintSet.RIGHT, (constraintRam.parent as View).id, ConstraintSet.RIGHT)
            constraintSet.connect(R.id.ramConstraintLayout, ConstraintSet.LEFT, R.id.cpuGridLayout, ConstraintSet.RIGHT)
            constraintSet.setVerticalBias(R.id.ramConstraintLayout, 0.496f)
        } else {
            constraintSet.setMargin(R.id.ramConstraintLayout, ConstraintSet.LEFT, resources.getDimension(R.dimen.ramConstraintMarginStart).toInt())
            constraintSet.connect(R.id.ramConstraintLayout, ConstraintSet.TOP, R.id.cpuGridLayout, ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.ramConstraintLayout, ConstraintSet.BOTTOM, (constraintRam.parent as View).id, ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.ramConstraintLayout, ConstraintSet.RIGHT, (constraintRam.parent as View).id, ConstraintSet.RIGHT)
            constraintSet.connect(R.id.ramConstraintLayout, ConstraintSet.LEFT, (constraintRam.parent as View).id, ConstraintSet.RIGHT)
        }


        //endregion
        //region cpuGrid
        constraintSet.constrainWidth(R.id.cpuGridLayout, resources.getDimension(R.dimen.cpuBarWidth).toInt() + resources.getDimension(R.dimen.cpuTextWidth).toInt())
        constraintSet.constrainHeight(R.id.cpuGridLayout, resources.getDimension(R.dimen.cpuBarHeight).toInt() * coreCount)

        constraintSet.setMargin(R.id.cpuGridLayout, ConstraintSet.TOP, resources.getDimension(R.dimen.cpuGridMarginTop).toInt())
        constraintSet.setMargin(R.id.cpuGridLayout, ConstraintSet.START, resources.getDimension(R.dimen.cpuGridMarginLeft).toInt())


        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            constraintSet.setMargin(R.id.cpuGridLayout, ConstraintSet.END, resources.getDimension(R.dimen.cpuGridMarginEnd).toInt())
            constraintSet.setMargin(R.id.cpuGridLayout, ConstraintSet.BOTTOM, resources.getDimension(R.dimen.cpuGridMarginBottom).toInt())
            constraintSet.connect(R.id.cpuGridLayout, ConstraintSet.TOP, (gridCpu.parent as View).id, ConstraintSet.TOP)
            constraintSet.connect(R.id.cpuGridLayout, ConstraintSet.BOTTOM, (gridCpu.parent as View).id, ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.cpuGridLayout, ConstraintSet.LEFT, (gridCpu.parent as View).id, ConstraintSet.LEFT)
            constraintSet.connect(R.id.cpuGridLayout, ConstraintSet.RIGHT, R.id.ramConstraintLayout, ConstraintSet.LEFT)
        } else {
            constraintSet.setMargin(R.id.cpuGridLayout, ConstraintSet.END, resources.getDimension(R.dimen.cpuGridMarginRight).toInt())
            constraintSet.setMargin(R.id.cpuGridLayout, ConstraintSet.BOTTOM, resources.getDimension(R.dimen.cpuGridMarginEnd).toInt())
            constraintSet.connect(R.id.cpuGridLayout, ConstraintSet.TOP, (gridCpu.parent as View).id, ConstraintSet.TOP)
            constraintSet.connect(R.id.cpuGridLayout, ConstraintSet.BOTTOM, R.id.ramConstraintLayout, ConstraintSet.TOP)
            constraintSet.connect(R.id.cpuGridLayout, ConstraintSet.LEFT, (gridCpu.parent as View).id, ConstraintSet.LEFT)
            constraintSet.connect(R.id.cpuGridLayout, ConstraintSet.RIGHT, (gridCpu.parent as View).id, ConstraintSet.RIGHT)
        }
        //endregion

        return constraintSet
    }

    private fun generateRAMConstraintSet(orientation: Int): ConstraintSet {
        val ramConstraintSet = ConstraintSet()

        ramConstraintSet.constrainWidth(R.id.ramProgress, resources.getDimension(R.dimen.ramProgressWidth).toInt())
        ramConstraintSet.constrainHeight(R.id.ramProgress, resources.getDimension(R.dimen.ramProgressHeight).toInt())
        //region RAM Progress
        ramConstraintSet.connect(R.id.ramProgress, ConstraintSet.TOP, (ramProgress.parent as View).id, ConstraintSet.TOP)
        ramConstraintSet.connect(R.id.ramProgress, ConstraintSet.LEFT, (ramProgress.parent as View).id, ConstraintSet.LEFT)
        ramConstraintSet.connect(R.id.ramProgress, ConstraintSet.RIGHT, (ramProgress.parent as View).id, ConstraintSet.RIGHT)
        //endregion

        ramConstraintSet.constrainWidth(R.id.ramText, resources.getDimension(R.dimen.ramTextWidth).toInt())
        ramConstraintSet.constrainHeight(R.id.ramText, resources.getDimension(R.dimen.ramTextHeight).toInt())
        //ramConstraintSet.setMargin(R.id.ramText, ConstraintSet.TOP, (int)getResources().getDimension(R.dimen.ramTextMarginTop));
        //ramConstraintSet.setMargin(R.id.ramText, ConstraintSet.LEFT, (int)getResources().getDimension(R.dimen.ramTextMarginLeft));
        //ramConstraintSet.setMargin(R.id.ramText, ConstraintSet.BOTTOM, (int)getResources().getDimension(R.dimen.ramTextMarginBottom));
        //ramConstraintSet.setMargin(R.id.ramText, ConstraintSet.RIGHT, (int)getResources().getDimension(R.dimen.ramTextMarginRight));

        //RAM Text inside circle
        ramConstraintSet.connect(R.id.ramText, ConstraintSet.LEFT, R.id.ramProgress, ConstraintSet.RIGHT)
        ramConstraintSet.connect(R.id.ramText, ConstraintSet.RIGHT, R.id.ramProgress, ConstraintSet.LEFT)
        ramConstraintSet.connect(R.id.ramText, ConstraintSet.TOP, R.id.ramProgress, ConstraintSet.BOTTOM)
        ramConstraintSet.connect(R.id.ramText, ConstraintSet.BOTTOM, R.id.ramProgress, ConstraintSet.TOP)

        ramConstraintSet.constrainWidth(R.id.ramDetails, resources.getDimension(R.dimen.ramDetailsWidth).toInt())
        ramConstraintSet.constrainHeight(R.id.ramDetails, resources.getDimension(R.dimen.ramDetailsHeight).toInt())

        //RAM Details below circle
        ramConstraintSet.connect(R.id.ramDetails, ConstraintSet.RIGHT, R.id.ramProgress, ConstraintSet.LEFT)
        ramConstraintSet.connect(R.id.ramDetails, ConstraintSet.LEFT, R.id.ramProgress, ConstraintSet.RIGHT)
        ramConstraintSet.connect(R.id.ramDetails, ConstraintSet.TOP, R.id.ramProgress, ConstraintSet.BOTTOM)
        ramConstraintSet.connect(R.id.ramDetails, ConstraintSet.BOTTOM, (percentText.parent as View).id, ConstraintSet.BOTTOM)
        //endregion

        return ramConstraintSet
    }

    private fun initAndStartUpdating() {
        ramProgress.setProgress(0.0f)
        updateThread = Thread(Runnable {
            while (!MainActivity.isConnectionClosed()) {
                var asyncSuccess = true
                runBlocking(CommonPool) {
                    val cpuUsages = MainActivity.serverFunctions.getAllCpuUsage(1000000L, 2).await()

                    val ramUsages = MainActivity.serverFunctions.getUsedTotalRamKB().await()

                    if (cpuUsages == null || ramUsages == null) {
                        asyncSuccess = false
                        Handler(Looper.getMainLooper()).post {
                            MainActivity.createDialog("Lost Connection", "Lost connection with the server", null, null, null, activity) //MainActivity.ButtonData("Reconnect", () -> )
                        }
                        return@runBlocking
                    }
                    val ramUsagesGB = DoubleArray(2, { CommonFormatting.KBToGB(ramUsages[it]) } )
                    Log.d(TAG, "ram: used  ${ramUsagesGB[0]} GB of ${ramUsagesGB[1]} GB")
                    Handler(Looper.getMainLooper()).post {
                        for (i in 0 until cpuUsages.size) {
                            coreProgressArray[i].progress = cpuUsages[i].toInt() //Update cpu progress bar
                            coreDetailsArray[i].text = CommonFormatting.formatCpuText(cpuUsages[i]) //update cpu text
                        }
                        ramProgress.setProgress((ramUsagesGB[0] / ramUsagesGB[1] * 100).toFloat())
                        val ramText = CommonFormatting.formatRAM(ramUsagesGB[0], ramUsagesGB[1])
                        ramDetails.text = if (ramText[0] == '0') Html.fromHtml("<font color='#FAFAFA'>0</font>${ramText.substring(1)}") else ramText
                        percentText.text = "${DecimalFormat("##0.0").format((ramUsagesGB[0] / ramUsagesGB[1] * 100.0))}%"
                    }
                }
                if (!asyncSuccess) break
                Thread.sleep(100)
            }
        }).startThread()
    }


    companion object {
        private val TAG = "Arbitrium:Graphs"

        fun newInstance(coreCount: Int): GraphsFragment {
            val fragment = GraphsFragment()
            val args = Bundle()
            args.putInt("cpuCores", coreCount)
            fragment.arguments = args
            return fragment
        }
    }
}