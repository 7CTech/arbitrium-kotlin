package ctech.arbitrium.common

import com.google.gson.reflect.TypeToken
import ctech.arbitrium.common.Util
import ctech.arbitrium.common.sendtypes.ReturnedSendType
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by ctech on 29/01/18 in Arbitrium
 */
class UtilTest {
    @Test
    fun removeLevel() {
        assertEquals(null, "foo", Util.removeLevel("bar<foo>", '<', '>'))
        assertEquals(null, "foo", Util.removeLevel("bar1foo2", '1', '2'))
        assertEquals(null, "foo<bar>", Util.removeLevel("bar<foo<bar>>", '<', '>'))
    }

    @Test
    fun toNumber() {
        assertEquals(Util.toNumberType(2.toByte(), "Int")::class, Int::class)
        assertEquals(Util.toNumberType(2.toShort(), "Int")::class, Int::class)
        assertEquals(Util.toNumberType(2.toInt(), "Int")::class, Int::class)
        assertEquals(Util.toNumberType(2.toLong(), "Int")::class, Int::class)
        assertEquals(Util.toNumberType(2.toFloat(), "Int")::class, Int::class)
        assertEquals(Util.toNumberType(2.toDouble(), "Int")::class, Int::class)

    }
}