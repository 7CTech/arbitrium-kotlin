package ctech.arbitrium.common

import org.junit.Assert
import org.junit.Test

/**
 * Created by ctech on 31/01/18 in Arbitrium
 */
class TypeHelperTest {
    @Test
    fun toSimpleString() {
        Assert.assertEquals(null, "ReturnedSendType<Integer>",
                TypeHelper.typeToSimpleString(TypeHelper.getReturn(Int::class)))
        Assert.assertEquals(null, "ReturnedSendType<ArrayList<Double>>",
                TypeHelper.typeToSimpleString(TypeHelper.getReturn(TypeHelper.getParameterized(ArrayList::class, Double::class))))
        Assert.assertEquals(null, "ReturnedSendType<int[]>", TypeHelper.typeToSimpleString(TypeHelper.getReturn(TypeHelper.getType(IntArray::class))))
    }
}