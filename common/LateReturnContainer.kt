package ctech.arbitrium.common

/**
 * Created by ctech on 15/12/17 in Arbitrium
 */
public class LateReturnContainer <T : Any> public constructor() {
    public lateinit var returned: T
    public var success: Boolean = true
}