package ctech.arbitrium.common.sendtypes

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.reflect.TypeToken
import ctech.arbitrium.common.ParameterTypePair
import ctech.arbitrium.common.TypeHelper
import ctech.arbitrium.common.Util
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import kotlin.reflect.KClass

public class FunctionSendType : SendType {
    public lateinit var functionName: String
    public var params: Array<ParameterTypePair<*>>? = null
    public lateinit var returnType: String
    public override lateinit var uid: String

    @Suppress("unused", "RedundantVisibilityModifier")
    public constructor() {
        //for gson
    }

    @Suppress("unused", "RedundantVisibilityModifier")
    public constructor(functionName: String, params: Array<ParameterTypePair<*>>?, type: Type) {
        this.functionName = functionName
        this.params = params
        this.returnType = Util.removeLevel(TypeHelper.typeToSimpleString(type), '<', '>')
        uid = Util.genUid()
    }

    public override fun toString(): String {
        var res = "<${this::class.simpleName}>$functionName("
        if (params != null) params?.forEachIndexed( { i, it -> res += it.toString(); if (i != params!!.size - 1) res += ", " } )
        res += ")"
        return res
    }
}