package ctech.arbitrium.common.sendtypes

import kotlin.reflect.KClass

/**
 * Created by ctech on 8/12/17 in Arbitrium
 */
public class ReturnedSendType<T>: SendType {

    public var returned: T? = null
    public override lateinit var uid: String

    @Suppress("RedundantVisibilityModifier", "unused")
    public constructor() {
        //for gson
    }

    @Suppress("unused", "RedundantVisibilityModifier")
    public constructor(returned: T?, uid: String) {
        this.returned = returned
        this.uid = uid
    }

    public override fun toString(): String {
        return "<${this::class.simpleName}>${returned.toString()}"
    }
}