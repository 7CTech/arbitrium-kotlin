package ctech.arbitrium.common

import java.util.concurrent.TimeoutException
import java.util.concurrent.atomic.AtomicBoolean

public class DelayedMap<K, V> {
    @Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
    data class ObjectNotify(var obj: Object = Object(), var notified: AtomicBoolean = AtomicBoolean(false))

    private val lockMap: MutableMap<K, ObjectNotify> = mutableMapOf()
    private val internalMap: MutableMap<K, V> = mutableMapOf()

    public fun put(key: K, value: V): DelayedMap<K, V> {
        synchronized(lockMap) {
            internalMap[key] = value
            if (lockMap[key] != null) {
                synchronized(lockMap[key]!!.obj) {
                    lockMap[key]!!.obj.notify()
                    lockMap[key]!!.notified.set(true)
                }
            }
        }
        return this
    }

    public fun getAndWait(key: K, timeout: Long): V {
        if (lockMap[key] == null) lockMap[key] = ObjectNotify()
        return synchronized(lockMap[key]!!.obj) {
            var res: V?
            do {
                res = internalMap[key]
                if(res == null) {
                    lockMap[key]!!.obj.wait(timeout)
                    if (!lockMap[key]!!.notified.get()) throw TimeoutException("wait timed out")
                }
            } while(res == null)
            lockMap.remove(key)
            internalMap.remove(key)
            res
        }
    }

}