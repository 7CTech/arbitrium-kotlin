package ctech.arbitrium.common

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.Arrays
import java.util.Random
import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties

/**
 * Created by ctech on 9/12/17 in Arbitrium
 */
object Util {
    private val symbols: Array<Char> = arrayOf('`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '\'', '"', ',', '.', '/', '?', '<', '>', '|')

    public fun genRandomHex(): String {
        val random = Random(Random().nextLong())
        val rand = random.nextInt(256)
        return Integer.toHexString(rand)
    }

    public fun getRandomSymbol(): Char {
        val random = Random(Random().nextLong())
        return symbols[random.nextInt(symbols.size)]
    }

    public fun genUid(): String {
        var res = ""
        for (i in 0 until 10) {
            res += genRandomHex()
            if (i != 9) res += getRandomSymbol()
        }
        return res
    }

    public fun removeLevel(str: String, l: Char, r: Char): String {
        return str.substring(str.indexOf(l) + 1, str.lastIndexOf(r))
    }

    public fun modifiyNumberTypePair(pair: ParameterTypePair<Number>) {
        val castedNum = Util.toNumberType(pair.parameter, pair.type)
        pair.parameter = castedNum
    }

    /*public fun toNumberTypePair(num: Number, type: String): ParameterTypePair<Number> {
        val castedNum = Util.toNumberType(num, type)
        return ParameterTypePair(castedNum, TypeHelper.getType(castedNum::class))
    }*/

    public fun toNumberType(num: Number, type: String): Number {
        return when (type) {
            "Byte" -> num.toByte()
            "Short" -> num.toShort()
            "Int" -> num.toInt()
            "Long" -> num.toLong()
            "Float" -> num.toFloat()
            "Double" -> num.toDouble()
            else -> 0
        }
    }

    public fun toNumberTypeArray(nums: JsonArray, type: String): Array<out Number> {
        val arrayList: ArrayList<Number> = ArrayList()
        nums.iterator().forEach {
            arrayList.add(it.asNumber)
        }
        return toNumberTypeArray(arrayList.toTypedArray(), type)
    }
    /**
     * Similar to [toNumberType], but handles arrays
     * @param type type of number. Can contain array declaration, not essential
     */
    public fun toNumberTypeArray(nums: Array<Number>, type: String): Array<out Number> {
        val rawType = if (type.endsWith("[]")) {
            type.dropLast(2)
        } else {
            type
        }
        when (rawType) {
            "Byte" -> {
                val resArr = ArrayList<Byte>()
                nums.forEach {
                    resArr.add(it.toByte())
                }
                return resArr.toTypedArray()
            }
            "Short" -> {
                val resArr = ArrayList<Short>()
                nums.forEach {
                    resArr.add(it.toShort())
                }
                return resArr.toTypedArray()
            }
            "Int" -> {
                val resArr = ArrayList<Int>()
                nums.forEach {
                    resArr.add(it.toInt())
                }
                return resArr.toTypedArray()
            }
            "Long" -> {
                val resArr = ArrayList<Byte>()
                nums.forEach {
                    resArr.add(it.toByte())
                }
                return resArr.toTypedArray()
            }
            "Float" -> {
                val resArr = ArrayList<Byte>()
                nums.forEach {
                    resArr.add(it.toByte())
                }
                return resArr.toTypedArray()
            }
            "Double" -> {
                val resArr = ArrayList<Byte>()
                nums.forEach {
                    resArr.add(it.toByte())
                }
                return resArr.toTypedArray()
            }
        }
        throw IllegalArgumentException("Specified type was not a number")
    }

    public fun checkHasSignature(json: JsonObject, klass: KClass<*>): Boolean {
        val jsonProps = mutableListOf<String>()
        json.keySet().forEach({
            jsonProps.add(it)
        })
        if (jsonProps.size != klass.memberProperties.size) return false

        klass.memberProperties.forEach {
            if (!jsonProps.contains(it.name)) return false
        }
        return true
    }

    public fun <T> deserializeJsonArray(jsonArray: JsonArray, clazz: Class<T>): Array<T> {
        val resArr = ArrayList<Any>(jsonArray.size())
        jsonArray.iterator().forEach {
            if (it.isJsonPrimitive) {
                val primitive = it.asJsonPrimitive
                when {
                    primitive.isBoolean -> resArr.add(primitive.asBoolean)
                    primitive.isNumber -> resArr.add(primitive.asNumber)
                    primitive.isString -> resArr.add(primitive.asString)
                }
            } else {
                TODO("sort out objects")
            }
        }
        println(Arrays.toString(resArr.toTypedArray()))
        return resArr.toTypedArray() as Array<T>
    }

    public fun getCorespondingNumberClass(type: String): Class<out Number> {
        @Suppress("UNCHECKED_CAST")
        return TypeToken.get(getCorespondingNumberType(type)).rawType as Class<out Number>
    }

    public fun getCorespondingNumberType(type: String): Type {
        if (!NumberTypeStrings.contains(type)) throw IllegalArgumentException("Supplied type was not a number")

        return TypeHelper.getBoxedType(when(type) {
            "Byte" -> Byte::class
            "Short" -> Short::class
            "Char" -> Char::class
            "Int" -> Int::class
            "Long" -> Long::class
            "Float" -> Float::class
            "Double" -> Double::class
            else -> throw IllegalArgumentException("Supplied type was not a number")
        })
    }

    public val NumberTypeStrings = arrayOf(
            "Byte",
            "Short",
            "Char",
            "Int",
            "Long",
            "Float",
            "Double"
    )
}