package ctech.arbitrium.common

import com.google.gson.*
import ctech.arbitrium.common.Util.NumberTypeStrings
import ctech.arbitrium.common.Util.deserializeJsonArray
import ctech.arbitrium.common.Util.getCorespondingNumberType
import java.lang.reflect.Type

public class ParameterTypePairDeserializer : JsonDeserializer<ParameterTypePair<*>> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ParameterTypePair<*> {
        val type = (json!! as JsonObject).get("type").asString
        val isArray = type.endsWith("[]")
        val parameter = (json as JsonObject).get("parameter")
        if (isArray && !parameter.isJsonArray) { //something is quite wrong here
            throw JsonParseException("Parameter is supposedly an array, but has not been parsed as such. Aborting")
        }

        //the special types (boolean, num and string)

        if (isBooleanType(type)) {
            return ParameterTypePair(parameter.asBoolean, Boolean::class.java)
        }


        if (isNumberType(type)) {
            return if (isArray) {
                ParameterTypePair(Util.toNumberTypeArray(parameter.asJsonArray, type), TypeHelper.getArray(getCorespondingNumberType(type)))
            } else ParameterTypePair(Util.toNumberType(parameter.asNumber, type), Number::class.java)
        }

        if (isStringType(type)) {
            return if (isArray) {
                ParameterTypePair(deserializeJsonArray(parameter.asJsonArray, String::class.java), TypeHelper.getArray(String::class))
            } else ParameterTypePair(parameter.asString, String::class.java)
        }

        //todo: reflections to handle objects


        throw UnsupportedOperationException("non-primitives currently aren't handled")

    }

    private fun isBooleanType(type: String): Boolean {
        //todo: arrays
        return type.equals("boolean", true)
    }

    private fun isNumberType(type: String): Boolean {
        val rawType: String = if (type.endsWith("[]")) {
            type.dropLast(2)
        } else {
            type
        }

        return NumberTypeStrings.contains(rawType)
    }

    private fun isStringType(type: String): Boolean {
        val rawType: String = if (type.endsWith("[]")) {
            type.dropLast(2)
        } else {
            type
        }
        return rawType == "String"
    }
}