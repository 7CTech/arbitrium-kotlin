package ctech.arbitrium.common

import kotlinx.coroutines.experimental.Deferred
import kotlin.reflect.KClass

/**
 * Created by ctech on 31/01/18 in Arbitrium
 */
object Extensions {
    public fun <T> Deferred<T>.then(fulfilled: (T) -> Unit, rejected: (Throwable) -> Unit) {
        this.invokeOnCompletion { if (it == null) fulfilled(this.getCompleted()) else rejected(it) }
    }

    public fun Thread.startThread(): Thread {
        this.start(); return this
    }

    public fun <T> ThreadLocal<T>.setThis(value: T): ThreadLocal<T> {
        this.set(value)
        return this
    }

    public fun Class<*>.getKClass(): KClass<*> {
        return this.kotlin
    }

    public fun <T> Array<T>.containsNulls(): Boolean {
        this.forEach {
            if (it == null) return true
        }
        return false
    }

    /*fun KType.getJavaType(): Type {
        return TypeToken.get((classifier as KClass<*>).java).type
    }*/
}