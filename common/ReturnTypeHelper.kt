package ctech.arbitrium.common

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.KClass

/**
 * Similar to a [TypeHelper] but every method starts with [TypeHelper.getReturn]
 */
object ReturnTypeHelper {
    public fun getType(kClass: KClass<*>): Type {
        return TypeHelper.getReturn(kClass)
    }

    public fun getType(kClass: Array<out KClass<*>>): Array<Type> {
        val array = TypeHelper.getType(kClass)
        array.forEachIndexed { index, it ->
            array[index] = TypeHelper.getReturn(it)
        }
        return array

    }

    public fun getParameterized(kClass: KClass<*>, vararg typeArguments: KClass<*>): Type {
        return TypeHelper.getReturn(TypeHelper.getParameterized(TypeHelper.getType(kClass), *typeArguments))
    }

    public fun getParameterized(kClass: KClass<*>, vararg typeArguments: Type): Type {
        return TypeHelper.getReturn(TypeHelper.getParameterized(TypeHelper.getType(kClass), *typeArguments))
    }

    public fun getParameterized(type: Type, vararg typeArguments: KClass<*>): Type {
        return TypeHelper.getReturn(TypeHelper.getParameterized(type, *typeArguments))
    }

    public fun getParameterized(type: Type, vararg typeArguments: Type): Type {
        return TypeHelper.getReturn(TypeHelper.getParameterized(type, *typeArguments))
    }

    public fun getArray(kClass: KClass<*>): Type {
        return TypeHelper.getReturn(TypeHelper.getArray(kClass))
    }

    public fun getArray(type: Type): Type {
        if (TypeToken.get(type).rawType.isPrimitive) {
            throw IllegalArgumentException("Cannot get array for primitive Type, use KClass instead or pass the boxed type")
        }
        return TypeHelper.getReturn(TypeHelper.getArray(type))
    }
}