package ctech.arbitrium.common

import java.lang.SecurityManager

class CustomSecurityManager : SecurityManager() {
    public fun getCallerClassSimpleName(depth: Int): String {
        return classContext[depth].simpleName
    }

    public fun getCallerClassName(depth: Int): String {
        return classContext[depth].name
    }
}