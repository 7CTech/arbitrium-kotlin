package ctech.arbitrium.common

import java.lang.reflect.Type

/**
 * Created by ctech on 15/12/17 in Arbitrium
 *
 * Class to hold a non-null value value, and it's type in a string format.
 */
class ParameterTypePair <T : Any> {
    public var parameter: T
    public var type: String

    public constructor(parameter: T, clazz: Class<T>) {
        this.parameter = parameter
        this.type = clazz.simpleName + if (clazz.isArray) "[]" else ""
    }

    public constructor(parameter: T, type: Type) {
        this.parameter = parameter
        this.type = TypeHelper.typeToSimpleString(type)
    }

    override fun toString(): String {
        return "$parameter: ${parameter::class.simpleName}${if (!(type.equals(parameter::class.simpleName, true))) "!" else ""}"
    }
}