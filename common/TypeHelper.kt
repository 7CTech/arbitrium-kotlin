package ctech.arbitrium.common

import com.google.gson.internal.`$Gson$Types`
import com.google.gson.reflect.TypeToken
import ctech.arbitrium.common.Extensions.getKClass
import ctech.arbitrium.common.sendtypes.ReturnedSendType
import java.lang.reflect.GenericArrayType
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.lang.reflect.WildcardType
import java.util.ArrayList
import kotlin.reflect.KClass
import kotlin.reflect.KType

/**
 * Created by ctech on 31/01/18 in Arbitrium
 */

public object TypeHelper {
    public fun typeToSimpleString(type: Type): String {
        @Suppress("RemoveRedundantBackticks") //Something is weird, cause these are very much needed
        val type0 = `$Gson$Types`.canonicalize(type)
        return when (type0) {
            is Class<*> -> type0.simpleName
            is TypeToken<*> -> type0.rawType.simpleName
            is ParameterizedType -> {
                val builder = StringBuilder()
                builder.append(TypeToken.get(type).rawType.simpleName).append("<").append(typeToSimpleString(type0.actualTypeArguments[0]))
                for (i in 1 until type0.actualTypeArguments.size) {
                    builder.append(", ").append(typeToSimpleString(type0.actualTypeArguments[i]))
                }
                builder.append(">").toString()
            }
            is GenericArrayType -> {
                return typeToSimpleString(type0.genericComponentType) + "[]"
            }
            is WildcardType -> {
                return when {
                    type0.lowerBounds[0] != null -> "? super " + typeToSimpleString(type0.lowerBounds[0])
                    type0.upperBounds[0] == Object::class.java -> "?"
                    else -> "? extends " + typeToSimpleString(type0.upperBounds[0])
                }
            }
            else -> ""
        }
    }

    public fun getBoxedKClass(kClass: KClass<*>): KClass<*> {
        return if (kClass.java.isPrimitive) kClass.javaObjectType.kotlin else kClass
    }

    public fun getBoxedType(kClass: KClass<*>): Type {
        return getType(kClass.javaObjectType.kotlin)
    }

    public fun getType(kClass: KClass<*>): Type {
        return TypeToken.get(kClass.java).type
    }

    public fun getBoxedType(kClass: Array<out KClass<*>>): Array<Type> {
        val typeArray: ArrayList<Type> = arrayListOf()
        kClass.forEach {
            typeArray.add(getBoxedType(it))
        }
        return typeArray.toTypedArray()
    }

    public fun getType(kClass: Array<out KClass<*>>): Array<Type> {
        val typeArray: ArrayList<Type> = arrayListOf()
        kClass.forEach {
            typeArray.add(getType(it))
        }
        return typeArray.toTypedArray()

    }

    public fun getParameterized(kClass: KClass<*>, vararg typeArguments: KClass<*>): Type {
        return getParameterized(getType(kClass), *typeArguments)
    }

    public fun getParameterized(kClass: KClass<*>, vararg typeArguments: Type): Type {
        return getParameterized(getType(kClass), *typeArguments)
    }

    public fun getParameterized(type: Type, vararg typeArguments: KClass<*>): Type {
        val actualTypeArguments: ArrayList<KClass<*>> = arrayListOf()

        typeArguments.forEach {
            if (it.java.isPrimitive) actualTypeArguments.add(it.javaObjectType.kotlin) else actualTypeArguments.add(it)
        }

        return getParameterized(type, *getType(actualTypeArguments.toTypedArray()))
    }

    public fun getParameterized(type: Type, vararg typeArguments: Type): Type {
        return TypeToken.getParameterized(type, *typeArguments).type
    }

    public fun getBoxedArray(kClass: KClass<*>): Type {
        return TypeToken.getArray(getBoxedType(kClass)).type
    }

    public fun getArray(kClass: KClass<*>): Type {
        return TypeToken.getArray(getType(kClass)).type
    }

    public fun getArray(type: Type): Type {
        return TypeToken.getArray(type).type
    }

    /*public fun getReturn(type: Type, vararg typeArguments: Type): Type {
        return getParameterized(ReturnedSendType::class, getParameterized(type, *typeArguments))
    }*/

    public fun getReturn(type: Type, vararg typeArguments: KClass<*>): Type {
        return getParameterized(ReturnedSendType::class, type, *getType(typeArguments))
    }

    /*public fun getReturn(kClass: KClass<*>, vararg typeArguments: Type): Type {
        return getParameterized(ReturnedSendType::class, getType(kClass), *typeArguments)
    }*/

    public fun getReturn(kClass: KClass<*>, vararg typeArguments: KClass<*>): Type {
        return getParameterized(ReturnedSendType::class, kClass, *typeArguments)
    }

    public fun typeToKClass(type: Type): KClass<out Any> {
        return TypeToken.get(type).rawType.getKClass()
    }

    /**
     * Used for [ParameterTypePair]s custom [deserializer][ParameterTypePairDeserializer]
     * The only handling here is for arrays, other than that no checks or fixes are made to the input
     */
    fun getNumberTypeFromString(type: String): Type? {
        val rawType: String
        val isArray: Boolean
        rawType = if (type.endsWith("[]")) { //array
            isArray = true
            type.dropLast(2)
        } else {
            isArray = false
            type
        }
        return when(rawType) {
            "byte", "Byte" -> {
                return if (isArray) {
                    TypeHelper.getBoxedArray(Byte::class)
                } else {
                    TypeHelper.getBoxedType(Byte::class)
                }
            }
            "short", "Short" -> {
                return if (isArray) {
                    TypeHelper.getBoxedArray(Byte::class)
                } else {
                    TypeHelper.getBoxedType(Byte::class)
                }
            }
            "int", "Integer" -> {
                return if (isArray) {
                    TypeHelper.getBoxedArray(Byte::class)
                } else {
                    TypeHelper.getBoxedType(Byte::class)
                }

            }
            "long", "Long" -> {
                return if (isArray) {
                    TypeHelper.getBoxedArray(Byte::class)
                } else {
                    TypeHelper.getBoxedType(Byte::class)
                }
            }
            "float", "Float" -> {
                return if (isArray) {
                    TypeHelper.getBoxedArray(Byte::class)
                } else {
                    TypeHelper.getBoxedType(Byte::class)
                }
            }
            "double", "Double" -> {
                return if (isArray) {
                    TypeHelper.getBoxedArray(Byte::class)
                } else {
                    TypeHelper.getBoxedType(Byte::class)
                }
            }
            else -> { null
            }
        }
    }
}