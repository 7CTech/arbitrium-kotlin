package ctech.arbitrium.common

/**
 * Created by ctech on 8/12/17 in Arbitrium
 */
public object SharedConstants {
    public const val PORT = 3008
    public const val BUF_LEN = 4096

    public const val LOCAL_FUN = "local"

    public object Messages {
        public const val PING = "ping"
        public const val PONG = "pong"
        public const val KILL = "kill"
        public const val MAP = "map"
    }
}


